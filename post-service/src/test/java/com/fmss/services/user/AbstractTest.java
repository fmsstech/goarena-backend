package com.fmss.services.user;

import com.fmss.services.common.feign.UserFeignClient;
import com.fmss.services.common.response.RestResponse;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.util.UserToken;
import com.fmss.services.common.util.UserThreadLocal;
import com.fmss.services.dto.post.PostDto;
import com.fmss.services.dto.post.PostState;
import com.fmss.services.dto.user.UserDto;
import com.fmss.services.post.mapper.PostMapper;
import com.fmss.services.post.persistence.entity.PostEntity;
import com.fmss.services.post.persistence.repository.PostRepository;
import com.fmss.services.post.service.PostService;
import org.junit.Before;
import org.mapstruct.factory.Mappers;
import org.mockito.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

/**
 * @author Fmss Tech
 */
public class AbstractTest {

	public static final Long POST_ID = 1l;
	public static final Long USER_ID = 1l;

	@InjectMocks
	PostService postService;

	@Mock
	PostRepository postRepository;

	@Spy
	PostMapper mapper = Mappers.getMapper(PostMapper.class);

	@Mock
	UserFeignClient userFeignClient;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		UserToken token = new UserToken();
		token.setId(USER_ID);
		UserThreadLocal.set(getMockUserDto());

		PostEntity userEntity = getMockPost();
		when(postRepository.findById(POST_ID)).thenReturn(Optional.ofNullable(userEntity));
		when(postRepository.save(Mockito.any(PostEntity.class))).thenReturn(userEntity);
		when(postRepository.findAll()).thenReturn(Arrays.asList(userEntity));
		when(postRepository.findAllByUserId(USER_ID)).thenReturn(Arrays.asList(userEntity));
		when(postRepository.findAllByStateOrderByCreatedDateDesc(Matchers.any(PostState.class))).thenReturn(Arrays.asList(userEntity));
		when(userFeignClient.getById(USER_ID)).thenReturn(getMockUserResponse());
		when(userFeignClient.getByIdList(Arrays.asList(USER_ID))).thenReturn(getUserListResponse());

	}

	public PostEntity getMockPost() {

		PostEntity post = new PostEntity();
		post.setImageStr("123123213123123mlsmdlsdsd");
		post.setTitle("new image");
		post.setId(POST_ID);
		post.setUserId(USER_ID);
		return post;
	}

	public PostDto getMockPostDto() {
		PostDto post = new PostDto();
		post.setImageStr("123123213123123mlsmdlsdsd");
		post.setTitle("new image");
		post.setId(POST_ID);
		return post;
	}

	public RestResponse<UserDto> getMockUserResponse() {
		return RestResponse.ok(ServiceResponse.success(getMockUserDto()));
	}

	public UserDto getMockUserDto() {

		UserDto user = new UserDto();
		user.setDeviceId("123123213");
		user.setUsername("FMSS");
		user.setId(USER_ID);
		user.setPassword("2345");
		return user;
	}

	public RestResponse<List<UserDto>> getUserListResponse() {
		ServiceResponse<List<UserDto>> success = ServiceResponse.success(Arrays.asList(getMockUserDto()));
		return RestResponse.ok(success);
	}

}

