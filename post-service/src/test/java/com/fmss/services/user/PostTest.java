package com.fmss.services.user;

import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.dto.post.*;
import com.fmss.services.dto.user.RegisterRequest;
import com.fmss.services.dto.user.UserDto;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author FMSS Tech
 */
@SpringBootTest
public class PostTest extends AbstractTest {

	@Test
	public void test_save_post() {
		PostDto mockPostDto = getMockPostDto();
		ServiceResponse<PostDto> post = postService.save(mockPostDto);
		assertNotNull(post);
		assertNotNull(post.getData());
		assertEquals(post.getStatus(), HttpStatus.OK);
	}

	@Test
	public void test_create_post() {
		MultipartFile file = new MockMultipartFile("test",new byte[]{});
		ServiceResponse<PostDto> post = postService.create("title", file);
		assertNotNull(post);
		assertNotNull(post.getData());
		assertEquals(post.getStatus(), HttpStatus.OK);
	}

	@Test
	public void test_find_all_by_state() {
		GetPostRequest request = new GetPostRequest();
		request.setState(PostState.CONFIRM.name());

		ServiceResponse<PostResponse> serviceResponse = postService.findAllByState(request);
		assertNotNull(serviceResponse);
		assertNotNull(serviceResponse.getData());
		assertEquals(serviceResponse.getStatus(), HttpStatus.OK);
		assertTrue(!serviceResponse.getData().getGroupList().isEmpty());
	}


	@Test
	public void test_change_state() {
		ChangePostRequest changePostRequest = new ChangePostRequest();
		changePostRequest.setPostId(POST_ID);
		changePostRequest.setState(PostState.CONFIRM);
		ServiceResponse<PostDto> serviceResponse = postService.changeState(changePostRequest);
		assertNotNull(serviceResponse);
		assertNotNull(serviceResponse.getData());
		assertEquals(serviceResponse.getStatus(), HttpStatus.OK);
	}

	@Test
	public void test_get_logged_user_posts() {
		ServiceResponse<List<PostDto>> serviceResponse = postService.getMy();
		assertNotNull(serviceResponse);
		assertNotNull(serviceResponse.getData());
		assertEquals(serviceResponse.getStatus(), HttpStatus.OK);
		assertTrue(!serviceResponse.getData().isEmpty());
	}

}

