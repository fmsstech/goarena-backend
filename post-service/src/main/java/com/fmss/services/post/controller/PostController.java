package com.fmss.services.post.controller;

import com.fmss.services.common.controller.AbstractController;
import com.fmss.services.common.response.RestResponse;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.service.RedisCacheService;
import com.fmss.services.dto.post.ChangePostRequest;
import com.fmss.services.dto.post.GetPostRequest;
import com.fmss.services.dto.post.PostDto;
import com.fmss.services.dto.post.PostState;
import com.fmss.services.post.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Fmss Tech
 */
@RestController
@RequestMapping("/post")
@AllArgsConstructor
public class PostController extends AbstractController<PostDto, PostService> {

	private final PostService service;
	private final RedisCacheService redisCacheService;

	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = "multipart/form-data")
	public RestResponse<PostDto> create(@RequestParam String text, @RequestPart("file") MultipartFile file) {
		ServiceResponse<PostDto> saved = service.create(text, file);
		return RestResponse.ok(saved);
	}

	@PostMapping({"/find-all-by-state"})
	public RestResponse finAllByState(@RequestBody GetPostRequest postRequest) {
		return RestResponse.ok(service.findAllByState(postRequest));
	}

	@PostMapping({"/find-all-confirmed"})
	public RestResponse findAll(@RequestBody GetPostRequest postRequest) {
		postRequest.setState(PostState.CONFIRM.name());
		return RestResponse.ok(service.findAllByState(postRequest));
	}

	@PostMapping({"/get-my-posts"})
	public RestResponse<List<PostDto>> getMyPost() {
		return RestResponse.ok(service.getMy());
	}

	@PostMapping({"/change-state"})
	public RestResponse<PostDto> changeState(@RequestBody ChangePostRequest postRequest) {
		return RestResponse.ok(service.changeState(postRequest));
	}

	@Override public PostService getService() {
		return service;
	}

}
