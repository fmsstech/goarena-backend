package com.fmss.services.post;

import com.fmss.services.common.config.GlobalConstants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
@EnableDiscoveryClient
@EntityScan(GlobalConstants.BASE_PACKAGE)
@ComponentScan({GlobalConstants.BASE_PACKAGE})
@EnableJpaRepositories(basePackages = GlobalConstants.BASE_PACKAGE)
@EnableFeignClients(basePackages = GlobalConstants.BASE_PACKAGE)
public class PostServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostServiceApplication.class, args);
	}

}
