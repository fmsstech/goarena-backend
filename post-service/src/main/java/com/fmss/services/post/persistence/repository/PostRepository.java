package com.fmss.services.post.persistence.repository;

import com.fmss.services.common.persistence.repository.AbstractRepository;
import com.fmss.services.dto.post.PostState;
import com.fmss.services.post.persistence.entity.PostEntity;

import java.util.List;

/**
 * @author FMSS TECH
 */
public interface PostRepository extends AbstractRepository<PostEntity> {

	List<PostEntity> findAllByStateOrderByCreatedDateDesc(PostState state);

	List<PostEntity> findAllByUserId(Long userId);
}

