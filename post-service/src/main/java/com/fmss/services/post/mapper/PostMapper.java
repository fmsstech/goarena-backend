package com.fmss.services.post.mapper;

import com.fmss.services.common.mapper.AbstractMapper;
import com.fmss.services.dto.post.PostDto;
import com.fmss.services.post.persistence.entity.PostEntity;
import org.mapstruct.Mapper;

/**
 * @author FMSS TECH
 */
@Mapper(componentModel = "spring")
public interface PostMapper extends AbstractMapper<PostEntity, PostDto> {

}

