package com.fmss.services.post.service;

import com.fmss.services.common.feign.UserFeignClient;
import com.fmss.services.common.mapper.AbstractMapper;
import com.fmss.services.common.persistence.repository.AbstractRepository;
import com.fmss.services.common.response.RestResponse;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.service.BaseService;
import com.fmss.services.common.util.UserThreadLocal;
import com.fmss.services.dto.post.*;
import com.fmss.services.dto.user.UserDto;
import com.fmss.services.post.mapper.PostMapper;
import com.fmss.services.post.persistence.entity.PostEntity;
import com.fmss.services.post.persistence.repository.PostRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author FMSS TECH
 */
@Service
@AllArgsConstructor
@Slf4j
public class PostService extends BaseService<PostEntity, PostDto> {

	private final PostRepository repository;
	private final PostMapper mapper;
	private final UserFeignClient userFeignClient;

	@Cacheable(cacheNames = "postCache")
	@Override public ServiceResponse<List<PostDto>> getAll() {
		return super.getAll();
	}

	@Override public ServiceResponse<PostDto> save(PostDto dto) {
		dto.setState(PostState.WAITING_CONFIRM);
		UserDto userToken = UserThreadLocal.get();
		dto.setUserId(userToken.getId());
		ServiceResponse<PostDto> save = null;
		try {
			return super.save(dto);
		} catch (Exception e) {
			return ServiceResponse.internalError("cannot save");

		}

	}

	public ServiceResponse<PostDto> create(String text, MultipartFile file) {
		UserDto userToken = UserThreadLocal.get();

		RestResponse<UserDto> userRR = userFeignClient.getById(userToken.getId());
		if (userRR.isSuccess()) {

			PostEntity postEntity = new PostEntity();
			postEntity.setState(PostState.WAITING_CONFIRM);
			postEntity.setTitle(text);
			postEntity.setUserId(userRR.getData().getId());

			try {
				String encodedString = Base64.getEncoder().encodeToString(file.getBytes());
				postEntity.setImageStr(encodedString);
			} catch (IOException e) {
				return ServiceResponse.internalError(e.getMessage());
			}

			PostEntity saved = repository.save(postEntity);

			return ServiceResponse.success(mapper.toDto(saved));

		} else {
			return ServiceResponse.notFound("user not found.");
		}

	}

	public ServiceResponse<PostResponse> findAllByState(GetPostRequest postRequest) {

		PostResponse pr = new PostResponse();
		PostState state = null;
		try {
			state = PostState.valueOf(postRequest.getState());
		} catch (IllegalArgumentException e) {
			state = null;
		}

		List<PostEntity> allByState = null;
		if (state == null) {
			allByState = repository.findAll();
		} else {
			allByState = repository.findAllByStateOrderByCreatedDateDesc(state);
		}

		List<Long> userIds = allByState.stream().map(p -> p.getUserId()).collect(Collectors.toList());
		RestResponse<List<UserDto>> users = userFeignClient.getByIdList(userIds);
		Map<Long, UserDto> umap = null;
		if (users.isSuccess()) {
			umap = users.getData().stream().collect(Collectors.toMap(u -> u.getId(), Function.identity(), (o, o2) -> o));
		}

		for (PostEntity postEntity : allByState) {
			PostGroup postResponse = new PostGroup();
			PostDto post = mapper.toDto(postEntity);
			postResponse.setPost(post);
			postResponse.setUser(umap != null ? umap.get(postEntity.getUserId()) : null);
			pr.getGroupList().add(postResponse);
		}

		return ServiceResponse.success(pr);
	}

	public ServiceResponse<PostDto> changeState(ChangePostRequest postRequest) {

		Optional<PostEntity> byId = repository.findById(postRequest.getPostId());
		if (byId.isPresent()) {
			PostEntity postEntity = byId.get();
			postEntity.setState(postRequest.getState());

			return ServiceResponse.success(mapper.toDto(repository.save(postEntity)));
		}

		return ServiceResponse.notFound();
	}

	public ServiceResponse<List<PostDto>> getMy() {
		UserDto userToken = UserThreadLocal.get();
		List<PostEntity> myPosts = repository.findAllByUserId(userToken.getId());

		List<PostDto> list = new ArrayList<>();
		for (PostEntity myPost : myPosts) {
			PostDto postDto = mapper.toDto(myPost);
			list.add(postDto);
		}
		return ServiceResponse.success(list);
	}

	@Override public AbstractRepository<PostEntity> getRepository() {
		return repository;
	}

	@Override public AbstractMapper<PostEntity, PostDto> getMapper() {
		return mapper;
	}
}

