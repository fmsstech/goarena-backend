package com.fmss.services.post.persistence.entity;

import com.fmss.services.common.persistence.entity.AbstractEntity;
import com.fmss.services.dto.post.PostState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author FMSS TECH
 */
@Entity(name = "Post")
@Data
@Builder
@Table(name = "post")
@NoArgsConstructor
@AllArgsConstructor
public class PostEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userId;

	@Enumerated(EnumType.STRING)
	private PostState state;

	private String title;

	private String description;

	@Column(length = 10485760)
	private String imageStr;

}

