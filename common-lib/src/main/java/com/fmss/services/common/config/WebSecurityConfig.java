package com.fmss.services.common.config;

import com.fmss.services.common.service.RedisCacheService;
import com.fmss.services.common.util.TokenUtils;
import com.fmss.services.common.util.UserThreadLocal;
import com.fmss.services.common.util.UserToken;
import com.fmss.services.dto.user.UserDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@AllArgsConstructor
public class WebSecurityConfig extends OncePerRequestFilter {

	private final RedisCacheService redisCacheService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

		Boolean isLogin = request.getServletPath().equalsIgnoreCase("/")
				|| request.getServletPath().contains("/login")
				//				|| request.getServletPath().contains("/logout")
				|| request.getServletPath().contains("/user/register/")
				|| request.getServletPath().contains("excel-template")
				|| request.getServletPath().contains("excel-upload")
				|| request.getServletPath().equalsIgnoreCase("/action-log")
				|| request.getServletPath().contains("/error")
				|| request.getServletPath().contains("/error-404")
				|| request.getServletPath().contains("register")
				|| request.getServletPath().contains("swagger")
				|| request.getServletPath().contains("actuator")
				|| request.getServletPath().contains("health")
				|| request.getServletPath()
				.equalsIgnoreCase("/v2/api-docs");

		if (isLogin) {
			filterChain.doFilter(request, response);
			return;
		}

		if ("OPTIONS".equals(request.getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			String token = TokenUtils.getTokenWithoutBearer(request);
			boolean existInBlackList = redisCacheService.checkTokenInBlackList(token);

			boolean equals1 = Boolean.TRUE.equals(TokenUtils.validateToken(token));

			if (!existInBlackList && StringUtils.isNotEmpty(token) && equals1) {

				UserToken userToken = TokenUtils.getUserToken(token);
				UserDto user = redisCacheService.getUser(userToken.getId());

				UserThreadLocal.set(user);

				filterChain.doFilter(request, response);

				UserThreadLocal.unset();

			}
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		}
	}

}
