package com.fmss.services.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TokenUtils {

	public static final String AUTH_TOKEN_NAME = "Authorization";
	public static final String USER = "user";

	private static Key getSigningKey() {
		String stringValue = "fmss.tech1235";
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(stringValue);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS512.getJcaName());
		return signingKey;
	}

	private static Claims getBody(String token) {
		return Jwts.parser().setSigningKey(TokenUtils.getSigningKey()).parseClaimsJws(token).getBody();
	}

	public static String getToken(HttpServletRequest request) {
		return request.getHeader(AUTH_TOKEN_NAME);
	}

	public static String getTokenWithoutBearer(HttpServletRequest request) {
		return StringUtils.isEmpty(TokenUtils.getToken(request)) ? null
				: TokenUtils.getToken(request).substring(7, TokenUtils.getToken(request).length());
	}

	public static UserToken getUserToken(HttpServletRequest request) {
		String tokenWithoutBearer = TokenUtils.getTokenWithoutBearer(request);
		if (StringUtils.isNotEmpty(tokenWithoutBearer)) {
			try {
				Claims body = TokenUtils.getBody(tokenWithoutBearer);
				String obj = (String) body.get(USER);

				return new ObjectMapper().readValue(obj, UserToken.class);
			} catch (Exception e) {
			}
		}
		return null;
	}

	public static UserToken getUserToken(String token) {
		if (StringUtils.isNotEmpty(token)) {
			try {
				Claims body = TokenUtils.getBody(token);
				String obj = (String) body.get(USER);

				return new ObjectMapper().readValue(obj, UserToken.class);
			} catch (Exception e) {
			}
		}
		return null;
	}

	public static String createToken(UserToken userResponse) {
		long expires = System.currentTimeMillis() + 3600000;
		Map<String, Object> claims = new HashMap<>();
		try {
			String json = new ObjectMapper().writeValueAsString(userResponse);
			claims.put(USER, json);
		} catch (JsonProcessingException e) {
		}

		return Jwts.builder().setClaims(claims).setExpiration(new Date(expires))
				.signWith(SignatureAlgorithm.HS512, getSigningKey()).compact();
	}

	public static boolean validateToken(String token) {
		try {
			Claims body = getBody(token);
			Date expiration = body.getExpiration();
			if (expiration.before(new Date())) {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
