package com.fmss.services.common.service;

import com.fmss.services.dto.user.UserDto;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RedisCacheService {

	private final String USER_CACHE = "USER";
	private final String BLACK_LIST_TOKEN_CACHE = "BLACK_LIST_TOKEN_CACHE";
	private RedisTemplate redisTemplate;

	public void addUser(UserDto userDto) {
		HashOperations hashOperations = redisTemplate.opsForHash();
		hashOperations.put(USER_CACHE, userDto.getId(), userDto);
	}

	public UserDto getUser(Long userId) {
		HashOperations hashOperations = redisTemplate.opsForHash();
		Object o = hashOperations.get(USER_CACHE, userId);
		if (o != null) {
			return (UserDto) o;
		}
		return null;
	}

	public void deleteUser(Long userId) {
		HashOperations hashOperations = redisTemplate.opsForHash();
		Object o = hashOperations.delete(USER_CACHE, userId);
	}

	public void addTokenToBlackList(String token) {
		HashOperations hashOperations = redisTemplate.opsForHash();
		hashOperations.put(BLACK_LIST_TOKEN_CACHE, token, Boolean.TRUE);
	}

	public boolean checkTokenInBlackList(String token) {
		HashOperations hashOperations = redisTemplate.opsForHash();
		Object resp = hashOperations.get(BLACK_LIST_TOKEN_CACHE, token);
		return resp != null;
	}

}
