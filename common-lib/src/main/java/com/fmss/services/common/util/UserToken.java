package com.fmss.services.common.util;

import lombok.Data;

/**
 * @author Fmss Tech
 */
@Data
public class UserToken {

	private Long id;
	private String name;

}

