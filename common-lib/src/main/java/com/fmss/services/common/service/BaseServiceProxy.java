package com.fmss.services.common.service;

import com.fmss.services.common.mapper.AbstractMapper;
import com.fmss.services.common.persistence.entity.AbstractEntity;
import com.fmss.services.common.persistence.repository.AbstractRepository;
import com.fmss.services.dto.common.AbstractDto;

public abstract class BaseServiceProxy<Entity extends AbstractEntity, DTO extends AbstractDto, Repository extends AbstractRepository<Entity>,
		M extends AbstractMapper<Entity, DTO>> {

	public abstract Repository getRepository();

	public abstract M getMapper();
}
