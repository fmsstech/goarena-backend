package com.fmss.services.common.util;

import org.apache.commons.lang3.BooleanUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author FMSS Tech
 */
public class CookieUtil {

	public static void setCookie(HttpServletResponse response, String key, String value, Boolean httponly) {
		Cookie cookie = new Cookie(key, value);
		cookie.setHttpOnly(BooleanUtils.isTrue(httponly));
		cookie.setPath("/");
		cookie.setMaxAge(60 * 60 * 24);// 1 day
		response.addCookie(cookie);
	}

	public static void deleteCookie(HttpServletResponse response, String key) {
		Cookie cookie = new Cookie(key, "");
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		cookie.setMaxAge(0);// 1 hour
		response.addCookie(cookie);
	}

	public static String getValueFromCookie(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();

		if (cookies != null && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equalsIgnoreCase(key)) {
					return cookie.getValue();
				}
			}
		}

		return "";
	}
}
