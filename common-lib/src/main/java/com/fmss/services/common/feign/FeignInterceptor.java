package com.fmss.services.common.feign;

import com.fmss.services.common.util.TokenUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author FMSS Tech
 */
@Component
public class FeignInterceptor implements RequestInterceptor {

	private final ApplicationContext context;

	@Autowired
	public FeignInterceptor(ApplicationContext context) {
		this.context = context;
	}

	@Override
	public void apply(RequestTemplate requestTemplate) {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if (requestAttributes == null) {
			return;
		}
		HttpServletRequest request = requestAttributes.getRequest();
		if (request == null) {
			return;
		}

		requestTemplate.header(TokenUtils.AUTH_TOKEN_NAME, request.getHeader(TokenUtils.AUTH_TOKEN_NAME));
	}

}
