//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fmss.services.common.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Data
@MappedSuperclass
@Inheritance(
		strategy = InheritanceType.TABLE_PER_CLASS
)
@EntityListeners({AuditingEntityListener.class})
public abstract class AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@CreatedDate
	@Column(
			name = "created_date",
			nullable = false,
			updatable = false
	)
	private ZonedDateTime createdDate = ZonedDateTime.now();
	@LastModifiedDate
	@Column(
			name = "last_modified_date"
	)
	@JsonIgnore
	private ZonedDateTime lastModifiedDate = ZonedDateTime.now();
	@CreatedBy
	@Column(
			name = "created_by",
			updatable = false
	)
	private String createdBy;

}
