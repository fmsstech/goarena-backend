package com.fmss.services.common.config;

public class GlobalConstants {

	public static final String BASE_PACKAGE = "com.fmss.services";

	private GlobalConstants() {
		throw new IllegalStateException("Constant class");
	}

}
