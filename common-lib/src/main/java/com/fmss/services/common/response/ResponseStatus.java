package com.fmss.services.common.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author Kemal Acar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseStatus {

	private String code = "0";
	private String message = "";

	ResponseStatus(String code) {
		setCode(code);
	}

	ResponseStatus(HttpStatus status, String message) {
		setCode(HttpStatus.OK.equals(status)?"0":"1");
		setMessage(message);
	}

	public boolean isSuccess(){
		return "0".equals(code);
	}

	public boolean isFail(){
		return !"0".equals(code);
	}
}

