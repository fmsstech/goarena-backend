package com.fmss.services.common.mapper;

import com.fmss.services.common.persistence.entity.AbstractEntity;
import com.fmss.services.dto.common.AbstractDto;

/**
 * @author Fmss Tech
 */
public interface AbstractMapper<Entity extends AbstractEntity, DTO extends AbstractDto> {

	DTO toDto(Entity paramEntity);

	Entity toEntity(DTO dto);
}

