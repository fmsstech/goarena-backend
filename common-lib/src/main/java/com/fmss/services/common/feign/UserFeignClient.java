package com.fmss.services.common.feign;

import com.fmss.services.common.response.RestResponse;
import com.fmss.services.dto.user.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "user-service")
public interface UserFeignClient {

	@GetMapping(value = "/user/{id}")
	public RestResponse<UserDto> getById(@PathVariable("id") Long id);

	@PostMapping(value = "/user/get-by-ids")
	public RestResponse<List<UserDto>> getByIdList(@RequestBody List<Long> idList);

}
