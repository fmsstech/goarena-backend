package com.fmss.services.common.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Fmss Tech
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RestResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private T data;
	private ResponseStatus status;

	public static RestResponse notFound() {
		return RestResponse.builder().status(new ResponseStatus("1")).build();
	}

	public static RestResponse unauthorized() {
		return RestResponse.builder().status(new ResponseStatus("1")).build();
	}

	public static RestResponse ok(ServiceResponse sr) {
		return RestResponse.builder().status(new ResponseStatus(sr.getStatus(), sr.getMessage())).data(sr.getData()).build();
	}

	public static <T> RestResponse<T> ok(T data) {
		return RestResponse.<T>builder().status(new ResponseStatus("", "")).data(data).build();
	}

	@JsonIgnore
	public boolean isSuccess() {
		return getStatus().isSuccess() && getData() != null;
	}

	@JsonIgnore
	public boolean isFail() {
		return getStatus().isFail() || getData() == null;
	}
}

