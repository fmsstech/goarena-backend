package com.fmss.services.common.util;

import com.fmss.services.dto.user.UserDto;

public class UserThreadLocal {

	public static final ThreadLocal<UserDto> userThreadLocal = new ThreadLocal<UserDto>();

	public static void set(UserDto context) {
		userThreadLocal.set(context);
	}

	public static void unset() {
		userThreadLocal.set(null);
	}

	public static UserDto get() {
		return userThreadLocal.get();
	}

}
