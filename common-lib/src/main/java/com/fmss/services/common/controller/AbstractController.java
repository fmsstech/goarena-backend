package com.fmss.services.common.controller;

import com.fmss.services.common.response.RestResponse;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.service.BaseService;
import com.fmss.services.dto.common.AbstractDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Fmss Tech
 */
@RestController
@Slf4j
public abstract class AbstractController<DTO extends AbstractDto, Service extends BaseService> {

	public abstract Service getService();

	@PostMapping
	public RestResponse<AbstractDto> save(@RequestBody @Valid DTO dto) {
		log.info(dto.toString());
		ServiceResponse save = getService().save(dto);
		return RestResponse.ok(save);
	}

	@GetMapping({"/{id}"})
	public RestResponse<AbstractDto> get(@Valid @PathVariable Long id) {
		log.info(id.toString());
		ServiceResponse abstractDto = getService().get(id);
		if (abstractDto == null) {
			return RestResponse.notFound();
		}

		return RestResponse.ok(abstractDto);

	}

	@DeleteMapping({"/{id}"})
	public RestResponse delete(@Valid @PathVariable Long id) {
		return RestResponse.ok(getService().remove(id));

	}

	@GetMapping({"/all"})
	public RestResponse<List<DTO>> getAll() {
		ServiceResponse<List<DTO>> all = getService().getAll();
		return RestResponse.ok(all);
	}

}

