package com.fmss.services.common.service;

import com.fmss.services.common.mapper.AbstractMapper;
import com.fmss.services.common.persistence.entity.AbstractEntity;
import com.fmss.services.common.persistence.repository.AbstractRepository;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.dto.common.AbstractDto;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Fmss Tech
 */
@Transactional
@Slf4j
public abstract class BaseService<Entity extends AbstractEntity, DTO extends AbstractDto>
		extends BaseServiceProxy<Entity, DTO, AbstractRepository<Entity>, AbstractMapper<Entity, DTO>> {

	public long count() {
		return getRepository().count();
	}

	public ServiceResponse<DTO> save(DTO dto) {
		Entity entity = getMapper().toEntity(dto);
		Entity save = getRepository().save(entity);
		return ServiceResponse.success(getMapper().toDto(save));
	}

	public ServiceResponse<DTO> get(Long id) {
		Optional<Entity> optional = getRepository().findById(id);
		if (optional.isPresent()) {
			Entity entity = optional.get();
			return ServiceResponse.success(getMapper().toDto(entity));
		}
		log.warn("entity not found.id=" + id);
		return ServiceResponse.notFound("Entity Not Found");
	}

	public ServiceResponse<Entity> getAsEntity(Long id) {
		Optional<Entity> optional = getRepository().findById(id);
		if (optional.isPresent()) {
			return ServiceResponse.success(optional.get());
		}
		log.warn("entity not found.id=" + id);
		return ServiceResponse.notFound();
	}

	public ServiceResponse<Void> remove(Long id) {

		try {
			getRepository().deleteById(id);
			return ServiceResponse.success(null);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ServiceResponse.internalError(e.getMessage());
		}
	}

	public ServiceResponse<List<DTO>> getAll() {
		List<Entity> all = getRepository().findAll();
		List<DTO> collected = all.stream().map(entity -> {
			return getMapper().toDto(entity);
		}).collect(Collectors.toList());
		return ServiceResponse.success(collected);
	}

}

