package com.fmss.services.common.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * @author Fmss Tech
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ServiceResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private T data;
	private HttpStatus status;
	private String message;

	public static ServiceResponse notFound(String message) {
		return ServiceResponse.builder().status(HttpStatus.NOT_FOUND).message(message).build();
	}

	public static ServiceResponse internalError(String message) {
		return ServiceResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR).message(message).build();
	}

	public static ServiceResponse status(HttpStatus status, String message) {
		return ServiceResponse.builder().status(status).message(message).build();
	}

	public static ServiceResponse notFound() {
		return ServiceResponse.builder().status(HttpStatus.NOT_FOUND).build();
	}

	public static <T> ServiceResponse<T> success(T data) {
		return ServiceResponse.<T>builder()
				.status(HttpStatus.OK)
				.message("OK")
				.data(data)
				.build();
	}

	public boolean isSuccess() {
		return HttpStatus.OK.equals(getStatus()) && getData() != null;
	}

	public boolean isFail() {
		return !HttpStatus.OK.equals(getStatus()) || getData() == null;
	}
}

