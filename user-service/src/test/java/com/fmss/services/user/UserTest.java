package com.fmss.services.user;

import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.util.TrippleDes;
import com.fmss.services.dto.user.RegisterRequest;
import com.fmss.services.dto.user.UserDto;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author FMSS Tech
 */
@SpringBootTest
public class UserTest extends AbstractTest {

	@Test
	public void test_find_userr_by_username_when_not_exist() {
		String testUsername = "fmss_not_exist";
		when(userRepository.findByUsername(testUsername)).thenReturn(Optional.ofNullable(null));
		ServiceResponse<UserDto> byUsername = userService.findByUsername(testUsername);
		assertNotNull(byUsername);
		assertEquals(byUsername.getStatus(), HttpStatus.NOT_FOUND);
		assertNull(byUsername.getData());
	}

	@Test
	public void test_find_customer_by_msisdn_when_exist() {
		ServiceResponse<UserDto> byMsisdn = userService.findByUsername(USERNAME);
		assertNotNull(byMsisdn);
		assertNotNull(byMsisdn.getData());

		assertNotNull(byMsisdn.getData().getId());
	}

	@Test
	public void test_find_by_username_and_password_success() throws Exception {
		ServiceResponse<UserDto> sr = userService.findByUsernameAndPassword(USERNAME, PASSWORD);
		assertNotNull(sr);
		assertNotNull(sr.getData());
		assertEquals(sr.getStatus(), HttpStatus.OK);
		assertNotNull(sr.getData().getId());
	}


	@Test
	public void test_find_by_username_and_password_fail() throws Exception {
		ServiceResponse<UserDto> sr = userService.findByUsernameAndPassword(USERNAME, "1234567");
		assertNotNull(sr);
		assertNull(sr.getData());
		assertEquals(sr.getStatus(), HttpStatus.NOT_FOUND);
	}



	@Test
	public void test_register_success() {
		RegisterRequest req = new RegisterRequest();
		req.setPassword(PASSWORD);
		req.setUsername(USERNAME);;
		ServiceResponse<UserDto> saved = userService.register(req);
		assertNotNull(saved);
		assertEquals(saved.getStatus(), HttpStatus.OK);
		assertNotNull(saved.getData());
		assertNotNull(saved.getData().getId());
	}

}

