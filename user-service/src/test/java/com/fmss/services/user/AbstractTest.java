package com.fmss.services.user;

import com.fmss.services.common.util.TrippleDes;
import com.fmss.services.dto.user.UserDto;
import com.fmss.services.user.mapper.UserMapper;
import com.fmss.services.user.persistence.entity.UserEntity;
import com.fmss.services.user.persistence.repository.UserRepository;
import com.fmss.services.user.service.UserService;
import org.junit.Before;
import org.mapstruct.factory.Mappers;
import org.mockito.*;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;

/**
 * @author Fmss Tech
 */
public class AbstractTest {

	public static final Long USER_ID = 1l;
	public static final String USERNAME = "fmss";
	public static final String PASSWORD = "12345";

	@InjectMocks
	UserService userService;

	@Mock
	UserRepository userRepository;

	@Spy
	UserMapper customerMapper = Mappers.getMapper(UserMapper.class);

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		UserEntity userEntity = getMockUser();
		when(userRepository.findById(USER_ID)).thenReturn(Optional.ofNullable(userEntity));
		when(userRepository.findByUsername(USERNAME)).thenReturn(Optional.ofNullable(userEntity));
		when(userRepository.save(Mockito.any(UserEntity.class))).thenReturn(userEntity);
		when(userRepository.findAll()).thenReturn(Arrays.asList(userEntity));
		when(userRepository.findByUsernameAndPassword(USERNAME,new TrippleDes().encrypt(PASSWORD))).thenReturn(Optional.ofNullable(userEntity));

	}

	public UserEntity getMockUser() {

		UserEntity user = new UserEntity();
		user.setDeviceId("123123213");
		user.setUsername(USERNAME);
		user.setId(USER_ID);
		user.setPassword(PASSWORD);
		return user;
	}

	public UserDto getMockUserDto() {

		UserDto user = new UserDto();
		user.setDeviceId("123123213");
		user.setUsername(USERNAME);
		user.setId(USER_ID);
		user.setPassword(PASSWORD);
		return user;
	}
}

