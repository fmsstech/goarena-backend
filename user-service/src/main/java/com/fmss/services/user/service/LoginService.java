package com.fmss.services.user.service;

import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.service.RedisCacheService;
import com.fmss.services.common.util.TokenUtils;
import com.fmss.services.common.util.UserThreadLocal;
import com.fmss.services.common.util.UserToken;
import com.fmss.services.dto.user.LoginResponse;
import com.fmss.services.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Fmss Tech
 */
@Component
@Slf4j
@AllArgsConstructor
public class LoginService {

	private final UserService userService;

	private final RedisCacheService redisCacheService;

	public ServiceResponse<LoginResponse> doLogin(String username, String password) {

		ServiceResponse<UserDto> byEmailSR = null;
		try {
			byEmailSR = userService.findByUsernameAndPassword(username, password);
		} catch (Exception e) {
			log.error("", e);
			return ServiceResponse.notFound("User not found!");
		}
		if (byEmailSR.isFail()) {
			return ServiceResponse.notFound("User not found!");
		} else {
			UserDto data = byEmailSR.getData();
			redisCacheService.addUser(data);
			ServiceResponse<LoginResponse> token = createToken(data);
			return token;
		}

	}

	private ServiceResponse<LoginResponse> createToken(UserDto userDto) {
		log.info("Create Token Service started");
		LoginResponse loginResponse = new LoginResponse();

		UserToken user = new UserToken();
		user.setId(userDto.getId());
		user.setName(userDto.getUsername());

		String xAuthToken = TokenUtils.createToken(user);
		loginResponse.setToken(xAuthToken);
		loginResponse.setUsername(user.getName());
		loginResponse.setId(user.getId());
		log.info("Create Token Service finished");
		return ServiceResponse.success(loginResponse);
	}

	public void logout(String token) {

		UserDto userToken = UserThreadLocal.get();
		redisCacheService.deleteUser(userToken.getId());
		redisCacheService.addTokenToBlackList(token);
	}
}

