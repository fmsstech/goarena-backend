package com.fmss.services.user.mapper;

import com.fmss.services.common.mapper.AbstractMapper;
import com.fmss.services.dto.user.RegisterRequest;
import com.fmss.services.dto.user.UserDto;
import com.fmss.services.user.persistence.entity.UserEntity;
import org.mapstruct.Mapper;

/**
 * @author FMSS TECH
 */
@Mapper(componentModel = "spring")
public interface UserMapper extends AbstractMapper<UserEntity, UserDto> {

	public UserDto toUserDto(RegisterRequest registerRequest);
}

