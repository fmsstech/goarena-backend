package com.fmss.services.user.persistence.repository;

import com.fmss.services.common.persistence.repository.AbstractRepository;
import com.fmss.services.user.persistence.entity.UserEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author FMSS TECH
 */
public interface UserRepository extends AbstractRepository<UserEntity> {

	public Optional<UserEntity> findByUsernameAndPassword(String username, String password);

	public Optional<UserEntity> findByUsername(String username);

	public List<UserEntity> findAllByIdIn(List<Long> idList);
}

