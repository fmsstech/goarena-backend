package com.fmss.services.user.service;

import com.fmss.services.common.mapper.AbstractMapper;
import com.fmss.services.common.persistence.repository.AbstractRepository;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.service.BaseService;
import com.fmss.services.common.util.TrippleDes;
import com.fmss.services.dto.user.RegisterRequest;
import com.fmss.services.dto.user.UserDto;
import com.fmss.services.user.mapper.UserMapper;
import com.fmss.services.user.persistence.entity.UserEntity;
import com.fmss.services.user.persistence.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author FMSS TECH
 */
@Service
@AllArgsConstructor
@Slf4j
public class UserService extends BaseService<UserEntity, UserDto> {

	private final UserRepository repository;
	private final UserMapper mapper;

	@Override public ServiceResponse<UserDto> save(UserDto dto) {
		try {
			TrippleDes td = new TrippleDes();
			String encrypted = td.encrypt(dto.getPassword());
			dto.setPassword(encrypted);
			return super.save(dto);
		} catch (Exception e) {
			log.error("", e);
			return ServiceResponse.internalError(e.getMessage());
		}
	}

	public ServiceResponse<UserDto> register(RegisterRequest registerRequest) {
		try {
			UserDto user = mapper.toUserDto(registerRequest);
			return save(user);
		} catch (Exception e) {
			log.error("", e);
			return ServiceResponse.internalError(e.getMessage());
		}
	}

	public ServiceResponse<UserDto> findByUsernameAndPassword(String email, String password) throws Exception {
		String encrypted = new TrippleDes().encrypt(password);

		Optional<UserEntity> byEmail = repository.findByUsernameAndPassword(email, encrypted);
		if (byEmail.isPresent()) {
			return ServiceResponse.success(mapper.toDto(byEmail.get()));
		} else {
			return ServiceResponse.status(HttpStatus.NOT_FOUND, "");
		}

	}

	public ServiceResponse<UserDto> findByUsername(String email) {
		Optional<UserEntity> byEmail = repository.findByUsername(email);
		if (byEmail.isPresent()) {
			return ServiceResponse.success(mapper.toDto(byEmail.get()));
		} else {
			return ServiceResponse.status(HttpStatus.NOT_FOUND, "");
		}
	}

	public ServiceResponse<List<UserDto>> findAllByIdList(List<Long> idList) {
		List<UserEntity> all = repository.findAllByIdIn(idList);
		return ServiceResponse.success(all.stream().map(u -> mapper.toDto(u)).collect(Collectors.toList()));
	}

	@Override public AbstractRepository<UserEntity> getRepository() {
		return repository;
	}

	@Override public AbstractMapper<UserEntity, UserDto> getMapper() {
		return mapper;
	}
}

