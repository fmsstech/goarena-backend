package com.fmss.services.user.persistence.entity;

import com.fmss.services.common.persistence.entity.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * @author FMSS TECH
 */
@Entity(name = "User")
@Data
@Builder
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private String username;

	@Column(unique = true)
	private String deviceId;

	@Column
	private String password;

	@CreatedDate
	private ZonedDateTime lastLogin = ZonedDateTime.now();

}

