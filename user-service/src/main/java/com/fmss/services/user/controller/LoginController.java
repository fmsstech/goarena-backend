package com.fmss.services.user.controller;

import com.fmss.services.common.response.RestResponse;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.common.util.TokenUtils;
import com.fmss.services.dto.user.LoginRequest;
import com.fmss.services.dto.user.LoginResponse;
import com.fmss.services.user.service.LoginService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Fmss Tech
 */
@RestController
@RequestMapping("/")
@AllArgsConstructor
public class LoginController {

	private final LoginService loginService;

	@PostMapping({"/login"})
	public RestResponse<LoginResponse> login(@RequestBody LoginRequest loginRequest, HttpServletResponse response) {
		ServiceResponse<LoginResponse> login = loginService.doLogin(loginRequest.getUsername(), loginRequest.getPassword());
		return RestResponse.ok(login);
	}

	@PostMapping({"/logout"})
	public RestResponse<LoginResponse> logout(HttpServletRequest request, HttpServletResponse response) {

		String tokenWithoutBearer = TokenUtils.getTokenWithoutBearer(request);
		loginService.logout(tokenWithoutBearer);

		return RestResponse.ok(new ServiceResponse());
	}
}

