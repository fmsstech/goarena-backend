package com.fmss.services.user.controller;

import com.fmss.services.common.controller.AbstractController;
import com.fmss.services.common.response.RestResponse;
import com.fmss.services.common.response.ServiceResponse;
import com.fmss.services.dto.user.RegisterRequest;
import com.fmss.services.dto.user.UserDto;
import com.fmss.services.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author FMSS TECH
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController extends AbstractController<UserDto, UserService> {

	private final UserService service;

	@PostMapping({"/register"})
	public RestResponse<UserDto> register(@RequestBody RegisterRequest registerRequest) {
		ServiceResponse<UserDto> saved = null;
		try {
			saved = service.register(registerRequest);
			return RestResponse.ok(saved);
		} catch (Exception e) {
			return RestResponse.ok(ServiceResponse.internalError("can not save"));
		}

	}

	@PostMapping({"/get-by-ids"})
	public RestResponse<List> register(@RequestBody List<Long> idList) {
		return RestResponse.ok(service.findAllByIdList(idList));
	}

	@Override public UserService getService() {
		return service;
	}

}

