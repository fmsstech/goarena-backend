
# Read Me First
The following was discovered as part of building this project:


# Getting Started
* Java/SpringBoot Microservices 

# Requirements

* Docker
 

#### Install & Run Microservices
build docker images of microservices
```bash 
docker build -t config-service .
```
```bash
docker build -t discovery-service .
```
```bash
docker build -t gateway-service .
```
```bash
docker build -t user-service .
```
```bash
docker build -t post-service .
```
start up  images by following command;

```bash
docker-compose up -d
```

