package com.fmss.services.dto.post;

/**
 * @author Fmss Tech
 */
public enum PostState {
	WAITING_CONFIRM, CONFIRM,IGNORE
}
