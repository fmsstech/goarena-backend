package com.fmss.services.dto.post;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kemal Acar
 */
@Data
public class PostResponse {

	private List<PostGroup> groupList = new ArrayList<>();

}

