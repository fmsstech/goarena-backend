package com.fmss.services.dto.post;

import com.fmss.services.dto.common.AbstractDto;
import lombok.Data;

import java.io.File;

/**
 * @author Fmss Tech
 */
@Data
public class PostRequest  {

	private String text;

}

