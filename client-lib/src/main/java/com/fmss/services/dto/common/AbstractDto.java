package com.fmss.services.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * @author Fmss Tech
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AbstractDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private ZonedDateTime createdDate = ZonedDateTime.now();
	private ZonedDateTime lastModifiedDate = ZonedDateTime.now();
	private String createdBy;
	private Long id;

}

