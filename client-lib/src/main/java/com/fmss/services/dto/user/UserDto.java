package com.fmss.services.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fmss.services.dto.common.AbstractDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

/**
 * @author FMSS TECH
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserDto extends AbstractDto {

	private static final long serialVersionUID = 1L;

	private Long id;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;

	private String username;

	private String deviceId;

	private ZonedDateTime lastLogin = ZonedDateTime.now();

}
