package com.fmss.services.dto.post;

import com.fmss.services.dto.common.AbstractDto;
import lombok.Data;

/**
 * @author Fmss Tech
 */
@Data
public class PostDto extends AbstractDto {

	private Long userId;

	private PostState state;

	private String title;

	private String description;;

	private String imageStr;

}

