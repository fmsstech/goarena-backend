package com.fmss.services.dto.user;

import lombok.Data;

/**
 * @author Fmss Tech
 */
@Data
public class RegisterRequest {

	private String username;
	private String password;
	private String deviceId;

}

