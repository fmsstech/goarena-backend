package com.fmss.services.dto.user;

import lombok.Data;

/**
 * @author Fmss Tech
 */
@Data
public class LoginRequest {

	private String username;
	private String password;
}

