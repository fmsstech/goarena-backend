package com.fmss.services.dto.post;

import com.fmss.services.dto.user.UserDto;
import lombok.Data;

/**
 * @author Kemal Acar
 */
@Data
public class PostGroup {
	private PostDto post;
	private UserDto user;

}

