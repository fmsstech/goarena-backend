package com.fmss.services.dto.post;

import lombok.Data;

/**
 * @author Fmss Tech
 */
@Data
public class ChangePostRequest {

	Long postId;
	PostState state;

}

