package com.fmss.services.dto.user;

import lombok.Data;

/**
 *  * @author Fmss Tech
 */
@Data
public class LoginResponse {

	private String token;
	private String username;
	private Long id;
}

