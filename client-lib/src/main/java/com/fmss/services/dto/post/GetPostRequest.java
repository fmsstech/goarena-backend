package com.fmss.services.dto.post;

import lombok.Data;

/**
 * @author Fmss Tech
 */
@Data
public class GetPostRequest {

	private int page = 1;
	private String state = "";

}

