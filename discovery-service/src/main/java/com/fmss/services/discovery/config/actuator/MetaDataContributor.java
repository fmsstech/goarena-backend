package com.fmss.services.discovery.config.actuator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class MetaDataContributor implements InfoContributor {

	private final ApplicationContext ctx;
	
	public MetaDataContributor(ApplicationContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void contribute(Info.Builder builder) {
		DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		Date result = new Date(ctx.getStartupDate());

		Map<String, Object> details = new HashMap<>();
		details.put("startupDate", simple.format(result));
		details.put("status", "UP");
		builder.withDetail("context", details);
	}
}
